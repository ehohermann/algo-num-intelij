package com.hermanneho.utilities;

public class Print {

    public static void stylePrintMin(String data) {

        System.out.println("\n_______________________________________\n");
        System.out.println("\t" + data + "\n");
        System.out.println("_______________________________________\n\n");
    }

    public static void stylePrint(String data) {

        System.out.println("\n_______________________________________________________\n");
        System.out.println("\t" + data + "\n");
        System.out.println("_______________________________________________________\n\n");
    }

    public static void stylePrintMax(String data) {
        System.out.println("\n \n___________________________________________________________________________________________________\n");
        System.out.println("\t" + data + "\n");
        System.out.println("___________________________________________________________________________________________________\n\n");

    }
}
