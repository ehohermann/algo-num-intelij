package com.hermanneho.linear.methodes;

import com.hermanneho.linear.utilities.Matrix;
import com.hermanneho.linear.utilities.abstrait.MatrixOperation;
import com.hermanneho.linear.utilities.MatrixSquare;
import com.hermanneho.linear.utilities.abstrait.MatrixStatus;
import com.hermanneho.utilities.Print;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Gauss {
    Matrix matrixA;
    Matrix matrixB;
    Matrix matrixAugmented; //matrixAugmented
    Matrix solution;
    /**
     * les resultats de la Matrix
     */
    ArrayList<Matrix> results = new ArrayList<Matrix>();

    public Gauss() {
        this.matrixA = new MatrixSquare(3, 3);
        this.matrixB = new Matrix(1, 3);
        this.matrixAugmented = MatrixOperation.getAugmented(matrixA, matrixB);
    }

    public Gauss(MatrixSquare matrixA, Matrix matrixB) {
        this.matrixA = matrixA;
        this.matrixB = matrixB;
        this.matrixAugmented = MatrixOperation.getAugmented(matrixA, matrixB);
    }
    public Gauss(Matrix matrixAugmented) {
        this.matrixAugmented = matrixAugmented;
    }

    public void printGauss() {
        /**
         * Affichage
         */
        System.out.println("Methode de Gauss sans pivot");
        System.out.println("Matrix A");
        this.matrixA.print();
        System.out.println("Matrix B");
        this.matrixB.print();
        System.out.println("Matrix Augmented");
        this.matrixAugmented.print();
    }


    public void exec() {

        int n = this.matrixA.getSize();
        int nbrColumn = this.matrixAugmented.getNbrColumn();
        int nbrLine = this.matrixAugmented.getNbrLine();
        this.results.add(this.matrixAugmented);

        System.out.println("\nDébut de la boucle de traitement pour k element");
        for (int k = 1; k <= n - 1; k++) {
            Matrix newResult = new Matrix(nbrColumn, nbrLine);
            for (int i = 1; i <= k; i++) {
                for (int j = 1; j <= n + 1; j++) {
                    newResult.setDataValueOf(i - 1, j - 1, results.get(k - 1).getDataValueOf((i - 1), (j - 1)));
                }
            }
            /**
             * verification des pivot
             */
            if (results.get(k - 1).getDataValueOf(k - 1, k - 1) != 0) {
                for (int i = k + 1; i <= n; i++) {
                    for (int j = 1; j <= k; j++) {
                        newResult.setDataValueOf(i - 1, j - 1, 0.0);
                    }
                }
                for (int i = k + 1; i <= n; i++) {
                    for (int j = k + 1; j <= n + 1; j++) {
                        double Aijk = results.get(k - 1).getDataValueOf(i - 1, j - 1);
                        double Aikk = results.get(k - 1).getDataValueOf(i - 1, k - 1);
                        double Akkk = results.get(k - 1).getDataValueOf(k - 1, k - 1);
                        double Akjk = results.get(k - 1).getDataValueOf(k - 1, j - 1);
                        double val = Aijk - (Aikk / Akkk * Akjk);
                        newResult.setDataValueOf(i - 1, j - 1, val);
                    }
                }
                this.results.add(newResult);
            } else {
                System.out.println("Pivot null la Matrix est donc ...");
                break;
            }

        }

        System.out.println("\nFin de la boucle de traitement pour k element");

        System.out.println("\nAffichage des Matrixs pour chaque element de k");
        for (int i = 0; i < this.results.size(); i++) {
            System.out.println("\nPour k = " + i);
            this.results.get(i).print();
        }
        remonte();
    }

    public void execGaussJordan() {

        int n = this.matrixA.getSize();
        int nbrColumn = this.matrixAugmented.getNbrColumn();
        int nbrLine = this.matrixAugmented.getNbrLine();
        this.results.add(this.matrixAugmented);

        Matrix newResult = new Matrix(nbrColumn, nbrLine);

        Matrix oldResult = this.matrixAugmented;
        System.out.println("\nMatrix Augmenter");
        oldResult.print();
        System.out.println("\nDébut de la boucle de traitement pour k element");
        for (int k = 1; k <= n; k++) {


            /**
             * Pivotage de la matrice OK
             */
            oldResult = MatrixOperation.pivotMatrice(n, k - 1, oldResult);

            /**
             * verification des pivot
             */
            Print.stylePrintMin("Affichage du resultat k " + k);

            if (MatrixStatus.swingNotNull(oldResult, k - 1)) {

                //Initialisation du nouveau pivot a 1
                //System.out.println("Pivot A" + k + k + " Initialisé a 1 ");

                double Akk = oldResult.getDataValueOf(k - 1, k - 1);
                newResult.setDataValueOf(k - 1, k - 1, 1);
                //System.out.println("\nDivision de la line k= " + k);
                //System.out.println(oldResult.getLine(k - 1));

                for (int i = k + 1; i <= n + 1; i++) {
                    //System.out.println(" \nColumn " + i + " Par le pivot A" + k + k);

                    double Aki = oldResult.getDataValueOf(k - 1, i - 1);
                    //System.out.println(" \n Aki= Aki / Akk: A" + k + i + " = A" + k + i + " / A" + k + k + "= " + Aki + " / " + Akk + " = " + (Aki / Akk));
                    newResult.setDataValueOf(k - 1, i - 1, Aki / Akk);
                    Aki = Aki / Akk;
                    for (int j = 1; j <= n; j++) {
                        if (j != k) {
                            //System.out.println("Ligne j= " + j + " Column i= " + i + " \n Aji - Aki * Ajk: A" + j + i + "=  A" + j + i + " - A" + k + i + " * A" + j + k);
                            double Aji = oldResult.getDataValueOf(j - 1, i - 1);
                            double Ajk = oldResult.getDataValueOf(j - 1, k - 1);
                            newResult.setDataValueOf(j - 1, i - 1, Aji - (Aki * Ajk));
                        }
                    }
                }
                for (int j = 1; j <= n; j++) {
                    if (j != k) {
                        //System.out.println("Ligne j= " + j + " Column k= " + k + " \n Ajk = 0 ");
                        newResult.setDataValueOf(j - 1, k - 1, 0);
                    }
                }
                System.out.println("\n");
                newResult.print();
                System.out.println("\n");
            } else {
                System.err.println("Pivot null la Matrix est donc ...");
                System.out.println("Position du pivot " + (k - 1));
                results.get(k - 1).print();
                System.out.println("Fin de l'Affichage de la matrice au pivot null");
                break;
            }
            System.out.println("Affectation de old result \n");
            oldResult = newResult;
            this.results.add(oldResult);
        }
        this.setSolution(oldResult);

        System.out.println("\nFin de la boucle de traitement pour k element");

        System.out.println("\nAffichage des Matrixs pour chaque element de k");
        for (int i = 0; i < this.results.size(); i++) {
            System.out.println("\nPour k = " + i);
            this.results.get(i).print();
        }
        //remonte();
    }

    /**
     * Fonction de remonté. C'est une fonction qui prend une mattrice augmenté en entré ou la matrice caré est une matrice diagonale supérieur.
     * Elle determine donc la matrice resultat de l'équation.
     */
    private void remonte() {
        int n;
        n = matrixA.getSize();
        Matrix solution = new Matrix(1, n);
        Matrix resulta = this.results.get(results.size() - 1);
        System.out.println("\nResultat = dernière Matrix");
        resulta.print();


        System.out.println("\nInitalisation de la remonté");
        double bn = resulta.getDataValueOf(n - 1, n);
        double ann = resulta.getDataValueOf(n - 1, n - 1);
        solution.setDataValueOf(n - 1, 0, (bn / ann));
        for (int i = n - 2; i >= 0; i--) {
            double somme = resulta.getDataValueOf(i, n);
            for (int j = i + 1; j < n; j++) {
                somme = somme - (resulta.getDataValueOf(i, j) * solution.getDataValueOf(j, 0));
            }
            double val = somme / (resulta.getDataValueOf(i, i));
            solution.setDataValueOf(i, 0, val);
        }
        System.out.println("\nFin de la remonté");
        System.out.println("\nAffichage du résultat de la remonté \n");
        solution.print();
        this.setSolution(solution);
    }

    public void execPartialPivot() {
        /**
         * nombre d'itération de l'algo
         */
        int n = this.matrixA.getSize();
        int nbrColumn = this.matrixAugmented.getNbrColumn();
        int nbrLine = this.matrixAugmented.getNbrLine();
        this.results.add(this.matrixAugmented);


        /**
         * ***********************************************************************
         *
         * Parcourt des lignes
         *
         * ************************************************************************
         */
        for (int k = 1; k <= n - 1; k++) {
            Matrix resNew = new Matrix(n + 1, n);

            /**
             * ***********************************************************************
             *
             * PIVOT DE LA Matrix *
             *
             * ************************************************************************
             */

            /**
             * ***********************************************************************
             *                                                                       *
             * determination du pivot max et de son index                            *
             *                                                                       *
             * ***********************************************************************
             */
            List l = new LinkedList<>();
            double max = 0;
            int indexe = k;
            for (int i = k; i <= n; i++) {
                //Parcourt de la matrice La colon est fixé avec k et on parcourt les i lignes en bas de k
                double val = Math.abs(this.results.get(k - 1).getDataValueOf(i - 1, k - 1));
                if (val > max) {
                    max = val;
                    indexe = i;
                }
            }
            Matrix resPivo = new Matrix(n + 1, n);
            /**
             * remplisage de la Matrix avec changement de pivot
             */

            resPivo.setData(this.results.get(k - 1).getData());

            /**
             * remplacement des lignes au niveau de la Matrix augmenté
             */
            if (indexe != k) {
                resPivo.changeDataLineByLine(indexe - 1, k - 1);
            }
            /**
             * ****************************************************************
             *
             * FIN DU PIVOT
             *
             * ****************************************************************
             */
            /**
             * ****************************************************************
             *
             * CALCUL DE LA NOUVEL Matrix
             *
             * ****************************************************************
             */
            /**
             * parcourt des lignes
             */
            for (int i = 1; i <= k; i++) {
                for (int j = 1; j <= n + 1; j++) {
                    resNew.setDataValueOf(i - 1, j - 1, resPivo.getDataValueOf((i - 1), (j - 1)));
                }
            }
            for (int i = k + 1; i <= n; i++) {
                for (int j = 1; j <= k; j++) {
                    resNew.setDataValueOf(i - 1, j - 1, 0.0);
                }
            }
            for (int i = k + 1; i <= n; i++) {

                double Aikk = resPivo.getDataValueOf(i - 1, k - 1);
                double Akkk = resPivo.getDataValueOf(k - 1, k - 1);
                double division = Aikk / Akkk;
                for (int j = k + 1; j <= n + 1; j++) {
                    double Aijk = resPivo.getDataValueOf(i - 1, j - 1);
                    double Akjk = resPivo.getDataValueOf(k - 1, j - 1);
                    double val = Aijk - (division * Akjk);
                    resNew.setDataValueOf(i - 1, j - 1, val);
                }
            }

            this.results.add(resNew);
        }


        remonte();
    }

    public Matrix getMatrixB() {
        return matrixB;
    }

    public void setMatrixB(Matrix matrixB) {
        this.matrixB = matrixB;
    }

    public Matrix getMatrixA() {
        return matrixA;
    }

    public void setMatrixA(Matrix matrixA) {
        this.matrixA = matrixA;
    }

    public Matrix getMatrixAugmented() {
        return this.matrixAugmented;
    }

    public void setMatrixAugmented(Matrix matrixAugmented) {
        this.matrixAugmented = matrixAugmented;
    }

    public Matrix getSolution() {
        return this.solution;
    }

    public void setSolution(Matrix solution) {
        this.solution = solution;
    }

    public ArrayList<Matrix> getResults() {
        return results;
    }

    public void setResults(ArrayList<Matrix> results) {
        this.results = results;
    }

    public void addResults(Matrix result) {
        this.results.add(result);
    }
}
