package com.hermanneho.linear.methodes;

import com.hermanneho.linear.utilities.Matrix;
import com.hermanneho.linear.utilities.MatrixSquare;
import com.hermanneho.linear.utilities.abstrait.MatrixOperation;
import com.hermanneho.utilities.Print;

import java.util.ArrayList;

public abstract class ExecMethods {
    public static Gauss execGauss(double[][] matrixSquaredData, double[] matrixColumnData) {

        Print.stylePrintMax("Execution de la methode de gauss sans pivot");
        ArrayList<Double> columnData = MatrixOperation.getDataFromArray(matrixColumnData);
        ArrayList<ArrayList<Double>> squareData = MatrixOperation.getDataFromArray(matrixSquaredData);
        MatrixSquare matrixSquare = new MatrixSquare(squareData);
        Matrix matrixColumn = new Matrix(columnData, 1);
        System.out.println("\nInitalisation des données de la methode de gauss");
        Gauss gauss = new Gauss(matrixSquare, matrixColumn);
        gauss.exec();
        return gauss;
    }

    public static Gauss execGaussPartial(double[][] matrixSquaredData, double[] matrixColumnData) {
        Print.stylePrintMax("Execution de la methode de gauss avec pivot partiel");
        ArrayList<Double> columnData = MatrixOperation.getDataFromArray(matrixColumnData);
        ArrayList<ArrayList<Double>> squareData = MatrixOperation.getDataFromArray(matrixSquaredData);
        MatrixSquare matrixSquare = new MatrixSquare(squareData);
        Matrix matrixColumn = new Matrix(columnData, 1);
        System.out.println("\nInitalisation des données de la methode de gauss");
        Gauss gauss = new Gauss(matrixSquare, matrixColumn);
        gauss.execPartialPivot();
        return gauss;
    }
    public static Gauss execGaussJordan(double[][] matrixSquaredData, double[] matrixColumnData) {

        Print.stylePrintMax("Execution de la methode de Gauss Jordan");
        ArrayList<Double> columnData = MatrixOperation.getDataFromArray(matrixColumnData);
        ArrayList<ArrayList<Double>> squareData = MatrixOperation.getDataFromArray(matrixSquaredData);
        MatrixSquare matrixSquare = new MatrixSquare(squareData);
        Matrix matrixColumn = new Matrix(columnData, 1);
        System.out.println("\nInitalisation des données de la methode de gauss");
        Gauss gauss = new Gauss(matrixSquare, matrixColumn);
        gauss.execGaussJordan();
        return gauss;
    }
}
