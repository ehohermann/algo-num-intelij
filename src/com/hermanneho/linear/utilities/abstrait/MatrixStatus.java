package com.hermanneho.linear.utilities.abstrait;

import com.hermanneho.linear.utilities.Matrix;

import java.util.ArrayList;

public class MatrixStatus {
    public static boolean isSquare() {
        return false;
    }

    public static boolean isColumn() {
        return false;
    }
    public static boolean isAugmented()
    {
        return false;
    }

    public static boolean isInvertible(Matrix matrix) {
        return false;
    }

    public static boolean swingNotNull(Matrix matrix, int swingIndex) {
        return matrix.getDataValueOf(swingIndex, swingIndex)!=0;
    }

    public static boolean isDominantDiagonal(Matrix matrix) {
        boolean result=true;
        for (int i = 0; i < matrix.getNbrLine(); i++) {
            double Aii=Math.abs(matrix.getDataValueOf(i,i));
            double somme=0;
            for (int j = 0; j < matrix.getNbrLine() && j!=i; j++) {
                somme +=Math.abs(matrix.getDataValueOf(i,j));
            }
            if (!(Aii >= somme) ){
                result=false;
            }
        }
        return result;
    }

    public static boolean haveDataset(ArrayList data) {
        return data.size() > 0;
    }

    public static boolean isValid(double[][] data) {
        boolean result = true;
        int size = data[0].length;
        for (double[] line : data) {
            if (size != line.length) {
                result = false;
            }
        }
        return result;
    }


    public static boolean isColumn(Matrix matrix) {
        return matrix.getNbrLine() == 1 || matrix.getNbrColumn() == 1;
    }
    /**
     * Checking matrice status
     */


    /**
     * check if the matrice data is set
     *
     * @return
     */
    public boolean isFilled(Matrix matrix) {
        boolean isFilled = true;
        if (matrix.getData() == null || matrix.getNbrColumn() == 0 || matrix.getNbrLine() == 0) {
            isFilled = false;
        }
        for (int i = 0; i < matrix.getData().size(); i++) {
            if (matrix.getData().get(i) == null) {
                isFilled = false;
            } else {

            }
        }
        //Dat not set yet
        return true;
    }

    /**
     * Check if the matrice is carré
     *
     * @return
     */
    public static boolean isSquare(Matrix matrix) {
        return matrix.getNbrColumn() == matrix.getNbrLine();
    }


    public static boolean isLine() {
        return false;
    }

    public static boolean isDiagonal() {
        return false;
    }

    public static boolean isTriangularSup() {
        return false;
    }

    public static boolean isTriangularInf() {
        return false;
    }

    public static boolean isUnity() {
        return false;
    }

    public static boolean isNull() {
        return false;
    }

    public static boolean isSymmetric() {
        return false;
    }

    public static boolean isAntiSymmetric() {
        return false;
    }
}