package com.hermanneho.linear.utilities.abstrait;

import com.hermanneho.linear.utilities.Matrix;

import java.util.ArrayList;

public abstract class MatrixOperation {
    /**
     * Add two matrice
     *
     * @param A
     * @param B
     * @return
     */
    public static Matrix addition(Matrix A, Matrix B) {
        return new Matrix(A.getNbrLine(), A.getNbrColumn());
    }

    public static ArrayList<Double> getDataFromArray(double[] a) {
        ArrayList<Double> line = new ArrayList<>();
        for (int i = 0; i < a.length; i++) {
            line.add(a[i]);
        }
        return line;
    }

    public static Matrix swingLine(Matrix matrix, int i, int j) {
        ArrayList<Double> tempon = matrix.getLine(i);
        matrix.setLine(i, matrix.getLine(j));
        matrix.setLine(j, tempon);
        return matrix;
    }

    public static Matrix pivotMatrice(int matrixSize, int indexPivot, Matrix matrixToPivot) {
        System.out.println("Pivotage de la matrice tail: " + matrixSize + " index : " + indexPivot);
        int maxIndex = indexPivot;
        for (int i = indexPivot + 1; i < matrixSize; i++) {
            if (Math.abs(matrixToPivot.getDataValueOf(maxIndex, indexPivot)) < Math.abs(matrixToPivot.getDataValueOf(i, indexPivot))) {
                maxIndex = i;
            }
        }
        System.out.println("Max index = " + maxIndex);
        if (maxIndex != indexPivot) {
            System.out.println("Pivot Effectuer");
            matrixToPivot = MatrixOperation.swingLine(matrixToPivot, maxIndex, indexPivot);
            matrixToPivot.print();
        } else {
            System.out.println("Pivot non Effectuer");
        }
        return matrixToPivot;
    }

    public static Matrix pivotMatrice(int indexPivot, Matrix matrixToPivot) {
        return MatrixOperation.pivotMatrice(indexPivot, indexPivot, matrixToPivot);
    }

    public static ArrayList<Double> divideLineBy(Matrix matrix, int lineIndex, double value) {
        ArrayList<Double> ligne = matrix.getLine(lineIndex);
        for (int i = 0; i < ligne.size(); i++) {
            ligne.set(i, ligne.get(i) / value);
        }
        return ligne;
    }

    public static ArrayList<Double> eliminatePivot(Matrix matrix, int pivotelineIndex, int lineToEliminateIndex, double value) {
        ArrayList<Double> linePivot = matrix.getLine(pivotelineIndex);
        ArrayList<Double> lineToEliminate = matrix.getLine(lineToEliminateIndex);
        ArrayList<Double> result = matrix.getLine(lineToEliminateIndex);
        for (int i = 0; i < linePivot.size(); i++) {
            result.set(i, lineToEliminate.get(i) - linePivot.get(i) * value);
        }
        return result;
    }


    public static ArrayList<ArrayList<Double>> getDataFromArray(double[][] a) {
        ArrayList<ArrayList<Double>> columns = new ArrayList<>();
        if (MatrixStatus.isValid(a)) {
            for (int i = 0; i < a.length; i++) {
                ArrayList<Double> line = new ArrayList<>();
                for (int j = 0; j < a[i].length; j++) {
                    line.add(a[i][j]);
                }
                columns.add(line);
            }
        } else {
            System.out.println("erreur Le tableau en entré est invalid");
            Error error = new Error();
            error.printStackTrace();
        }
        return columns;
    }

    public static ArrayList<ArrayList<Double>> fillData(int nbrLine, int nbrColumn) {
        ArrayList<ArrayList<Double>> data = new ArrayList<>();
        for (int i = 0; i < nbrLine; i++) {
            ArrayList<Double> line = new ArrayList<>();
            for (int j = 0; j < nbrColumn; j++) {
                line.add(0.0);
            }
            data.add(line);
        }
        return data;
    }

    public static void fillData(Matrix matrix) {

        double[][] matrice =
                {
                        {0, 0, 0},
                        {3, 4, 0},
                        {6, 0, 0}
                };
        ArrayList<ArrayList<Double>> data = MatrixOperation.getDataFromArray(matrice);
    }

    public static boolean isValid(Matrix matrix) {
        boolean result = true;
        int nbrLine = matrix.getNbrLine();
        int nbrColumn = matrix.getNbrColumn();
        if (matrix.getData().size() != nbrLine) {
            result = false;
        }
        for (ArrayList<Double> line : matrix.getData()) {
            if (line.size() != nbrColumn) {
                result = false;
                break;
            }
        }
        return result;
    }

    public static Matrix getAugmented(Matrix matrixSquare, Matrix matrixColumn) {
        Matrix matrix = new Matrix(3, 3);
        if (MatrixStatus.isSquare(matrixSquare) && MatrixStatus.isColumn(matrixColumn)) {
            //Debug
            //System.out.println(matrixSquare.getSize());
            matrix = new Matrix(matrixSquare.getSize() + matrixColumn.getNbrColumn(), matrixSquare.getSize());
            //debug
            //matrix.print();
            for (int i = 0; i < matrixSquare.getSize(); i++) {
                for (int j = 0; j < matrixSquare.getSize(); j++) {
                    matrix.setDataValueOf(i, j, matrixSquare.getDataValueOf(i, j));
                }
                matrix.setDataValueOf(i, matrixSquare.getSize(), matrixColumn.getDataValueOf(i, 0));
            }
            return matrix;
        } else {

            if (!MatrixStatus.isSquare(matrixSquare)) {
                System.err.println("La matrice A n'est pas carré");
            }
            if (!MatrixStatus.isColumn(matrixColumn)) {
                System.err.println("La matrice B n'est pas une matrice column");
            }
            Error error = new Error();
            error.printStackTrace();
        }
        return matrix;
    }
}
