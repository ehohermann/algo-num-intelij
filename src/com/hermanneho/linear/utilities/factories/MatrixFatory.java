package com.hermanneho.linear.utilities.factories;

import com.hermanneho.linear.utilities.Matrix;
import com.hermanneho.linear.utilities.MatrixSquare;
import com.hermanneho.linear.utilities.abstrait.MatrixOperation;

import java.util.ArrayList;

public class MatrixFatory {

    public static Matrix makeMatrix(double[][] arrayMatrix) {
        return new Matrix(arrayMatrix);
    }

    public static MatrixSquare makeMatrixSquare(double[][] arrayMatrix) {

        ArrayList<ArrayList<Double>> squareData = MatrixOperation.getDataFromArray(arrayMatrix);
        return new MatrixSquare(squareData);
    }

    public static Matrix makeMatrixColumn(double[] arrayMatrixColumn) {
        ArrayList<Double> columnData = MatrixOperation.getDataFromArray(arrayMatrixColumn);
        return new Matrix(columnData, 1);
    }

    public static MatrixSquare getMatrixSquare(int index) {

        double[][][] squareMatrixList = {
                {
                        {2, 1, 2},
                        {6, 4, 0},
                        {8, 5, 1}
                },
        };
        return new MatrixSquare(squareMatrixList[index]);
    }

    public static Matrix getMatrixColumn(int index) {
        double[][] columnMatrixList = {
                {
                        10, 26, 35
                },
        };
        ArrayList<Double> columnData = MatrixOperation.getDataFromArray(columnMatrixList[index]);
        return new Matrix(columnData, 1);
    }

    public static Matrix getMatrixAugmented(int index){
        return new Matrix();
    }

    public static ArrayList getGetMatrixSquareData() {
        return new ArrayList();
    }
}
