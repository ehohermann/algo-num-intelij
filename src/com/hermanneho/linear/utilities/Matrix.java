package com.hermanneho.linear.utilities;

import com.hermanneho.linear.utilities.abstrait.MatrixOperation;
import com.hermanneho.linear.utilities.abstrait.MatrixStatus;
import com.hermanneho.linear.utilities.interfaces.MatrixInterface;

import java.util.ArrayList;

public class Matrix implements MatrixInterface {
    protected ArrayList<ArrayList<Double>> data;
    protected int nbrColumn = 0;
    protected int nbrLine = 0;

    /**
     * The data is a liste of line
     * exemple data.get(i) return a line
     *
     * @return ArrayList<ArrayList < Double>>
     */
    public ArrayList<ArrayList<Double>> getData() {
        return data;
    }

    public Matrix() {

        this.data = new ArrayList<>();
        this.nbrColumn = 3;
        this.nbrLine = 3;
        this.data = MatrixOperation.fillData(nbrLine, nbrColumn);
    }

    public Matrix(int nbrColumn, int nbrLine) {
        this.data = new ArrayList<>();
        this.nbrColumn = nbrColumn;
        this.nbrLine = nbrLine;
        this.data = MatrixOperation.fillData(nbrLine, nbrColumn);
    }

    public Matrix(ArrayList<ArrayList<Double>> data) {
        if (MatrixStatus.haveDataset(data)) {
            this.data = data;
            this.nbrColumn = data.size();
            this.nbrLine = data.get(0).size();
        }
    }

    public Matrix(ArrayList<Double> data, int size) {
        //TODO implement methode to create any type off matrix with this methode
        if (MatrixStatus.haveDataset(data)) {
            this.data = new ArrayList<>();
            for (double dataValue : data) {
                ArrayList<Double> line = new ArrayList<>();
                line.add(dataValue);
                this.data.add(line);
            }
            this.nbrColumn = size;
            this.nbrLine = data.size();
        }
    }

    public Matrix(double[][] data) {
        this.setData(MatrixOperation.getDataFromArray(data));
    }

    public void setData(ArrayList<ArrayList<Double>> data) {
        if (MatrixStatus.haveDataset(data)) {
            this.data = data;
        }
    }


    public void fillData() {
        //if size of line or column is set
        ArrayList data = new ArrayList<>();
        for (int i = 0; i < this.getNbrLine(); i++) {
            ArrayList<Double> line = new ArrayList();
            for (int j = 0; j < this.getNbrColumn(); j++) {
                line.add(0.0);
            }
            data.add(line);
        }
        /*
        if (this.getSize() > 0 || this.getNbrLine() > 0 || this.getNbrColumn() > 0) {
            ArrayList data = new ArrayList<>();
            //add of lines
            if (this.getNbrLine() == 1 && this.getNbrColumn() > 1) {
                ArrayList<Double> line = new ArrayList();
                for (int i = 0; i < this.getNbrColumn(); i++) {
                    line.add(0.0);
                }
                data.add(line);
            }
            //add of column
            if (this.getNbrLine() >= 1 && this.getNbrColumn() == 1) {
                ArrayList<Double> column = new ArrayList();
                for (int i = 0; i < this.getNbrColumn(); i++) {
                    column.add(0.0);
                }
                data.add(column);
            }
            //add of matrix
            if (this.getNbrLine() > 1 && this.getNbrColumn() > 1) {
                ArrayList<Double> line = new ArrayList();
                for (int i = 0; i < this.getNbrColumn(); i++) {
                    line.add(0.0);
                }
                data.add(line);
            }
        }*/

        this.print();
    }

    public void setData(double[][] data) {
        this.setData(MatrixOperation.getDataFromArray(data));
    }

    public double getDataValueOf(int line, int column) {
        if (MatrixOperation.isValid(this)) {
            try {
                return this.getData().get(line).get(column);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public void setDataValueOf(int line, int column, double value) {
        try {
            this.getData().get(line).set(column, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void changeDataLineByLine(int lineToChangeIndex, int secondLineIndex) {
        ArrayList<Double> lineToChange=this.data.get(lineToChangeIndex);
        ArrayList<Double> secondLine=this.data.get(secondLineIndex);
        this.data.set(lineToChangeIndex, secondLine);
        this.data.set(secondLineIndex, lineToChange);
    }

    ;

    public int getNbrColumn() {
        return nbrColumn;
    }

    public void setNbrColumn(int nbrColumn) {
        this.nbrColumn = nbrColumn;
    }

    public int getNbrLine() {
        return nbrLine;
    }

    public void setNbrLine(int nbrLine) {
        this.nbrLine = nbrLine;
    }

    @Override
    public int getSize() {
        return this.nbrColumn;
    }

    @Override
    public void setSize(int size) {
        this.nbrColumn = size;
        this.nbrLine = size;
    }

    @Override
    public ArrayList<Double> getLine(int i) {
        return this.getData().get(i);
    }

    @Override
    public void setLine(int linePosition, ArrayList<Double> line) {
        this.getData().set(linePosition, line);
    }

    @Override
    public void addLine(ArrayList<Double> line) {
        this.getData().add(line);

    }

    @Override
    public void setColumn(int columnPosition, ArrayList<Double> column) {

    }

    @Override
    public ArrayList<Double> getColumn(int i) {
        return null;
    }

    @Override
    public void addColumn(ArrayList<Double> column) {

    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (int y1 = 0; y1 < this.nbrLine; y1++) {
            for (int x1 = 0; x1 < this.nbrColumn; x1++) {
                str.append(" ").append(this.getDataValueOf(y1, x1)).append(" ");
            }
            str.append("\n");
        }
        str.append("]");
        return str.toString();
    }

    public void print() {
/*
        System.out.println("Type de matrix : Indefini");
        System.out.println("Nombre de ligne " + this.getNbrLine());
        System.out.println("Nombre de colone " + this.getNbrColumn());*/
        if (MatrixOperation.isValid(this)) {
            System.out.println("Matrix : \n" + this);
        }
    }

}
