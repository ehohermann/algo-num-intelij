package com.hermanneho.linear.utilities.interfaces;

import java.util.ArrayList;

public interface MatrixInterface {
    /**
     * Receive  the column at a the specify position i
     * @param i
     * @return
     */
    public ArrayList<Double> getColumn(int i);
    /**
     * Receive the line at a the specify position i
     * @param i
     * @return
     */
    public ArrayList<Double> getLine(int i);

    /**
     * add a colum to the end off the matrice
     * @param column
     */
    public void addColumn(ArrayList<Double> column);

    /**
     * add a Line to the end off the matrice
     * @param line
     */
    public void addLine(ArrayList<Double> line);
    public void setColumn(int columnPosition, ArrayList<Double> column);
    public void setLine(int linePosition, ArrayList<Double> line);

    int getSize();
    void setSize(int size);
}
