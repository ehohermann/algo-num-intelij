package com.hermanneho.linear.utilities.interfaces;

public interface MatrixSquareInterface {
    int getDimention();
    void setDimention(int dimention);
}
