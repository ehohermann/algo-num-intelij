package com.hermanneho.linear.utilities;

import java.util.ArrayList;

public class MatrixSquare extends Matrix {

    protected int size = 0;

    public MatrixSquare() {
        super(3, 3);
        this.size = 3;
    }

    public MatrixSquare(ArrayList<ArrayList<Double>> data) {
        this.setData(data);
        this.setSize(data.size());
    }

    public MatrixSquare(double[][] data) {
        super(data);
    }

    public MatrixSquare(int nbrColumn, int nbrLine) {
        super(nbrColumn, nbrLine);
        this.size = nbrColumn;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
        this.setNbrColumn(size);
        this.setNbrLine(size);
    }

    public boolean isMatrixSquare() {
        return true;
    }
}
