package com.hermanneho.analysis.utilities;

import com.hermanneho.exception.EqualBorneException;

import java.util.Scanner;
public class Intervale {

    /**
     * Borne inf
     */
    private double inf;
    /**
     * Borne sup
     */
    private double sup;

    /**
     * contructeur qui demande a l'utilisateur de remplire l'intervale
     */
    public Intervale() {
        Scanner sc = new Scanner(System.in);
        System.out.println("entrez la valeur de a la borne inférieur");
        this.inf = sc.nextDouble();
        do {
            System.out.println("entrez la valeur de b la borne supérieur");
            this.sup = sc.nextDouble();
            if (this.sup < this.inf) {
                System.out.println("la valeur de B doit être supérieur à cel de a");
            }
        } while (this.sup < this.inf);
    }

    /**
     *
     * @param inf=Borne inf
     * @param sup =Borne Sup
     * @throws EqualBorneException
     */
    public Intervale(double inf, double sup) throws EqualBorneException {
        if (inf < sup) {
            this.inf = inf;
            this.sup = sup;
        } else if (inf > sup) {
            this.inf = sup;
            this.sup = inf;
        } else if (inf == inf) {
            throw new EqualBorneException("les Bornes A et B sont identiques");
        }
    }

    public double getInf() {
        return inf;
    }

    public void setInf(double inf) {
        this.inf = inf;
    }

    public double getSup() {
        return sup;
    }

    public void setSup(double sup) {
        this.sup = sup;
    }

    @Override
    public String toString() {
        return "Intervale{" + "inf=" + inf + ", sup=" + sup + '}';
    }

}
