package com.hermanneho.exception;

public class OverflowException  extends Exception {

    public OverflowException() {
        System.out.println("Vous essayer d'aceder a un coordonné qui n'apartient pas a la matrice");
    }

    public OverflowException(String message) {
        super(message);
    }
}
