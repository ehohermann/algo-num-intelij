package com.hermanneho.exception;

public class EqualBorneException extends Exception {

    public EqualBorneException() {
        System.out.println("Vous essayer d'aceder a un coordonné qui n'apartient pas a la matrice");
    }

    public EqualBorneException(String message) {
        super(message);
    }
}