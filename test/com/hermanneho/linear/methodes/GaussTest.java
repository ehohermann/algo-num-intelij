package com.hermanneho.linear.methodes;

import com.hermanneho.linear.utilities.Matrix;
import com.hermanneho.linear.utilities.MatrixSquare;
import com.hermanneho.linear.utilities.abstrait.MatrixOperation;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class GaussTest {

    @Test
    void printGauss() {
    }

    @Test
    void exec() {
        try {

            double[][] arrayMatrix =
                    {
                            {2, 1, 2},
                            {6, 4, 0},
                            {8, 5, 1}
                    };
            double[] arrayMatrixColumn = {
                    10, 26, 35
            };
            Gauss gauss=ExecMethods.execGauss(arrayMatrix, arrayMatrixColumn);

            assertEquals(3,gauss.solution.getDataValueOf(0,0));
            assertEquals(2,gauss.solution.getDataValueOf(1,0));
            assertEquals(1,gauss.solution.getDataValueOf(2,0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    void execPartial() {
        try {

            double[][] arrayMatrix =
                    {
                            {2, 1, 2},
                            {6, 4, 0},
                            {8, 5, 1}
                    };
            double[] arrayMatrixColumn = {
                    10, 26, 35
            };
            Gauss gauss=ExecMethods.execGaussPartial(arrayMatrix, arrayMatrixColumn);
            System.out.println("affichage de la solution");
            gauss.solution.print();

            assertEquals(3,gauss.solution.getDataValueOf(0,0));
            assertEquals(2,gauss.solution.getDataValueOf(1,0));
            assertEquals(1,gauss.solution.getDataValueOf(2,0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    void execGaussJordan() {
        try {

            double[][] arrayMatrix =
                    {
                            {2, -1, 1},
                            {-1, -1, -1},
                            {3, 3, 1}
                    };
            double[] arrayMatrixColumn = {
                    5, -2, 2
            };
            Gauss gauss=ExecMethods.execGaussJordan(arrayMatrix, arrayMatrixColumn);
  //          System.out.println("affichage de la solution");
//            gauss.solution.print();
/*
            assertEquals(3,gauss.solution.getDataValueOf(0,0));
            assertEquals(2,gauss.solution.getDataValueOf(1,0));
            assertEquals(1,gauss.solution.getDataValueOf(2,0));*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
/*
    @Test
    void getMatrixB() {
    }

    @Test
    void setMatrixB() {
    }

    @Test
    void getMatrixA() {
    }

    @Test
    void setMatrixA() {
    }

    @Test
    void getMatrixAugmented() {
    }

    @Test
    void setMatrixAugmented() {
    }

    @Test
    void getResults() {
    }

    @Test
    void setResults() {
    }

    @Test
    void addResults() {
    }*/
}