package com.hermanneho.linear.utilities.abstrait;

import com.hermanneho.linear.utilities.Matrix;
import com.hermanneho.linear.utilities.MatrixSquare;
import com.hermanneho.utilities.Print;
import org.junit.jupiter.api.Test;

import javax.swing.text.AttributeSet;
import javax.swing.text.Style;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class MatrixOperationTest {

    @Test
    void addition() {
    }

    @Test
    void getDataFromArray() {
    }

    @Test
    void swingLine() {

        Print.stylePrintMax("Test Matrix opération swing line");
        double[][] arrayMatrix =
                {
                        {2, 1, 2},
                        {6, 4, 0},
                        {8, 5, 1}
                };
        double[] arrayMatrixColumn = {
                10, 26, 35
        };


        ArrayList<Double> columnData = MatrixOperation.getDataFromArray(arrayMatrixColumn);
        ArrayList<ArrayList<Double>> squareData = MatrixOperation.getDataFromArray(arrayMatrix);
        MatrixSquare matrixSquare = new MatrixSquare(squareData);
        Matrix matrixColumn = new Matrix(columnData, 1);

        matrixSquare.print();
        MatrixOperation.swingLine(matrixSquare, 0,2);
        matrixSquare.print();
        matrixColumn.print();
        MatrixOperation.swingLine(matrixColumn, 0,2);
        matrixColumn.print();

        assertEquals(8,matrixSquare.getDataValueOf(0,0));
        assertEquals(5,matrixSquare.getDataValueOf(0,1));
        assertEquals(1,matrixSquare.getDataValueOf(0,2));

        assertEquals(2,matrixSquare.getDataValueOf(2,0));
        assertEquals(1,matrixSquare.getDataValueOf(2,1));
        assertEquals(2,matrixSquare.getDataValueOf(2,2));
        Print.stylePrint("Test Finished : Matrix opération swing line ");
    }

    @Test
    void testGetDataFromArray() {
    }

    @Test
    void fillData() {
    }

    @Test
    void testFillData() {
    }

    @Test
    void isValid() {
    }

    @Test
    void getAugmented() {
    }
}