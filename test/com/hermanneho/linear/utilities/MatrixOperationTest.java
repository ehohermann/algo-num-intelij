package com.hermanneho.linear.utilities;

import com.hermanneho.linear.utilities.abstrait.MatrixOperation;
import com.hermanneho.linear.utilities.abstrait.MatrixStatus;
import com.hermanneho.linear.utilities.factories.MatrixFatory;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class MatrixOperationTest {

    @Test
    void pivotMatrice() {

        double[][] arrayMatrix =
                {
                        {2, 1, 2},
                        {6, 4, 0},
                        {8, 5, 1}
                };
        double[] arrayMatrixColumn = {
                10, 26, 35
        };
        double [][][] squareMatrixList= {
                arrayMatrix
        };
        ArrayList list= new ArrayList();
        list.add(arrayMatrix);
        System.out.println(list.get(0));
    }
    @Test
    void getAugmented() {


        double[][] arrayMatrix =
                {
                        {2, 1, 2},
                        {6, 4, 0},
                        {8, 5, 1}
                };
        double[] arrayMatrixColumn = {
                10, 26, 35
        };
        double [][][] squareMatrixList= {
                arrayMatrix
        };
        ArrayList list= new ArrayList();
        list.add(arrayMatrix);
        System.out.println(list);
        //MatrixFatory.getGetMatrixSquareData().get(1);

        ArrayList<Double> columnData = MatrixOperation.getDataFromArray(arrayMatrixColumn);
        ArrayList<ArrayList<Double>> squareData = MatrixOperation.getDataFromArray(arrayMatrix);
        MatrixSquare matrixSquare = new MatrixSquare(squareData);
        Matrix matrixColumn = new Matrix(columnData, 1);
        Matrix matrixAugmented=MatrixOperation.getAugmented(matrixSquare, matrixColumn);

        //Print of data set for debug

         matrixColumn.print();
         matrixSquare.print();
         matrixAugmented.print();
        /**
         * Assertions
         */
        assertEquals(2, matrixAugmented.getData().get(0).get(0));
        assertEquals(1, matrixAugmented.getData().get(0).get(1));
        assertEquals(2, matrixAugmented.getData().get(0).get(2));
        assertEquals(10, matrixAugmented.getData().get(0).get(3));
        assertEquals(6, matrixAugmented.getData().get(1).get(0));
        assertEquals(4, matrixAugmented.getData().get(1).get(1));
        assertEquals(0, matrixAugmented.getData().get(1).get(2));
        assertEquals(26, matrixAugmented.getData().get(1).get(3));
        assertEquals(8, matrixAugmented.getData().get(2).get(0));
        assertEquals(5, matrixAugmented.getData().get(2).get(1));
        assertEquals(1, matrixAugmented.getData().get(2).get(2));
        assertEquals(35, matrixAugmented.getData().get(2).get(3));
    }

    @Test
    void haveDataset() {
        ArrayList<ArrayList<Double>> data = new ArrayList<>();
        assertFalse(MatrixStatus.haveDataset(data));
        ArrayList<Double> ligne = new ArrayList<>();
        ligne.add(1.0);
        data.add(ligne);
        assertTrue(MatrixStatus.haveDataset(data));
    }

    @Test
    void getDataFromArray() {
        double[][] matrice =
                {
                        {0, 1, 2},
                        {3, 4, 5},
                        {6, 7, 8}
                };
        ArrayList<ArrayList<Double>> data = MatrixOperation.getDataFromArray(matrice);
        assertTrue(MatrixStatus.isValid(matrice));
        assertEquals(0, data.get(0).get(0));
        assertEquals(1, data.get(0).get(1));
        assertEquals(2, data.get(0).get(2));
        assertEquals(3, data.get(1).get(0));
        assertEquals(4, data.get(1).get(1));
        assertEquals(5, data.get(1).get(2));
        assertEquals(6, data.get(2).get(0));
        assertEquals(7, data.get(2).get(1));
        assertEquals(8, data.get(2).get(2));

    }

    @Test
    void isValid() {
        double[][] matrice =
                {
                        {0, 1, 4, 3}, // tableau [0] de int
                        {5, 7, 9, 11, 13, 15, 17} // tableau [1] de int
                };
        assertFalse(MatrixStatus.isValid(matrice));
        double[][] matrice2 =
                {
                        {0, 1, 4, 3}, // tableau [0] de int
                        {5, 7, 9, 11} // tableau [1] de int
                };
        assertTrue(MatrixStatus.isValid(matrice2));
    }
}