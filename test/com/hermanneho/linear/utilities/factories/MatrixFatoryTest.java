package com.hermanneho.linear.utilities.factories;

import com.hermanneho.linear.utilities.Matrix;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixFatoryTest {

    @Test
    void makeMatrix() {

        double[][] arrayMatrix =
                {
                        {2, 1, 2},
                        {6, 4, 0},
                        {8, 5, 1}
                };

        Matrix matrix= MatrixFatory.makeMatrix(arrayMatrix);
        /**
         * Assertions
         */
        assertEquals(2, matrix.getData().get(0).get(0));
        assertEquals(1, matrix.getData().get(0).get(1));
        assertEquals(2, matrix.getData().get(0).get(2));

        assertEquals(6, matrix.getData().get(1).get(0));
        assertEquals(4, matrix.getData().get(1).get(1));
        assertEquals(0, matrix.getData().get(1).get(2));

        assertEquals(8, matrix.getData().get(2).get(0));
        assertEquals(5, matrix.getData().get(2).get(1));
        assertEquals(1, matrix.getData().get(2).get(2));

    }

    @Test
    void makeMatrixSquare() {
        double[][] arrayMatrix =
                {
                        {2, 1, 2},
                        {6, 4, 0},
                        {8, 5, 1}
                };
        Matrix matrix=  MatrixFatory.makeMatrixSquare(arrayMatrix);

        /**
         * Assertions
         */
        assertEquals(2, matrix.getDataValueOf(0, 0));
        assertEquals(1, matrix.getDataValueOf(0, 1));
        assertEquals(2, matrix.getDataValueOf(0, 2));

        assertEquals(6, matrix.getDataValueOf(1, 0));
        assertEquals(4, matrix.getDataValueOf(1, 1));
        assertEquals(0, matrix.getDataValueOf(1, 2));

        assertEquals(8, matrix.getDataValueOf(2, 0));
        assertEquals(5, matrix.getDataValueOf(2, 1));
        assertEquals(1, matrix.getDataValueOf(2, 2));

    }

    @Test
    void makeMatrixColumn() {
        double[] arrayMatrixColumn = {
                10, 26, 35
        };
        Matrix matrix=  MatrixFatory.makeMatrixColumn(arrayMatrixColumn);
        assertEquals(10, matrix.getDataValueOf(0, 0));
        assertEquals(26, matrix.getDataValueOf(1, 0));
        assertEquals(35, matrix.getDataValueOf(2, 0));
    }
}